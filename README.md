# OSM Watch
Rebirth OSM for android smartwatch with simple and progressive disign.
# Clean Architecture
I try to use clean Architecture
# Room
Using Room database for data cash
# MVVM
Google Jetpack
# Dependency Injection
Dagger 2
# RxKotlin
Using rx for work with not UI thread
# Navigation
Navigation Architecture Component
